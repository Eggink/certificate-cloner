###############################################################
# Scriptname: Certificate Cloner
# Author:     Mark Eggink
# Version:    1.0.1
# GitRepo:    https://gitlab.com/Eggink/certificate-cloner/      
###############################################################
   
# Thumbprint check
cd cert:\LocalMachine\My
ls | select-object NotAfter,Subject,Thumbprint

# Thumbprint paste
set-location -Path "cert:\LocalMachine\My"
$Thumbprint = (get-childitem -Path "Paste thumbprint from the old certificate here without quotes")

New-SelfSignedCertificate -CloneCert $Thumbprint

#Certificaat export
Export-Certificate -Cert Cert:\LocalMachine\My\"new thembprint here without quotes" -FilePath "Path to exportfile with extension .cer"